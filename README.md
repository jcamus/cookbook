# Cookbook

Mock project in order to get started with Git.
Coupled with the 'Getting Started with Git' presentation for INGENIANCE teams.

## Prerequisites

[Git for Windows](https://gitforwindows.org/) installed on your computer.

## Authors

* **Julien Camus** - [jcamus](https://gitlab.com/jcamus)

Based on Paolo Perrotta's work on Pluralsight: [How Git Works](https://app.pluralsight.com/library/courses/how-git-works/table-of-contents) and [Mastering Git](https://app.pluralsight.com/library/courses/mastering-git/table-of-contents)

## Acknowledgments

* Paolo Perrotta for his great presentations available on Pluralsight (see links above).